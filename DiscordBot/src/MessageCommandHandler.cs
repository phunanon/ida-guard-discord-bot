using DiscordBot.Commands;

namespace DiscordBot {

    public class MessageCommandHandler {
        // Local command storage
        private readonly Dictionary<string, IMessageCommand> _commands = new();

        public void StoreCommandsLocally() {
            // Storing command object locally
            AddCommand(new List<IMessageCommand> {
                new ExecuteInsitux(),
            });
        }

        private void AddCommand(List<IMessageCommand> commands) {
            // Adding command name + object locally
            foreach (var command in commands) {
                _commands.Add(command.Command.Name, command);
            }
        }

        public async Task Install(DiscordSocketClient client, SocketGuild guild) {
            // Iterating through stored slashCommandBuilderObjects and loading them to Discord
            foreach (var command in _commands.Values.ToList()) {
                await client.Rest.CreateGuildCommand(command.Command.Build(), guild.Id);
            }
        }

        public Task HandleCommandAsync(SocketMessageCommand command) {
            // Searching for the command locally and executing it if found
            if (_commands.TryGetValue(command.Data.Name, out var cmd))
            {
                cmd.Execute(command);
            }

            return Task.CompletedTask;
        }
    }
}
