using DiscordBot.Commands;

namespace DiscordBot {

    public class SlashCommandHandler {
        // Local command storage
        private readonly IDictionary<string, ISlashCommand> _commands = new Dictionary<string, ISlashCommand>();

        public void StoreCommandsLocally() {
            // Storing command object locally
            AddCommand(new List<ISlashCommand> {
                new GetRoles(),
            });
        }

        private void AddCommand(List<ISlashCommand> commands) {
            // Adding command name + object locally
            foreach (var command in commands) {
                _commands.Add(command.Command.Name, command);
            }
        }

        public async Task Install(DiscordSocketClient client, SocketGuild guild) {
            // Iterating through stored slashCommandBuilderObjects and loading them to Discord
            foreach (var command in _commands.Values.ToList()) {
                await guild.CreateApplicationCommandAsync(command.Command.Build());
            }
        }

        public Task HandleCommandAsync(SocketSlashCommand command) {
            // Searching for the command locally and executing it if found
            if (_commands.TryGetValue(command.Data.Name, out var cmd)) {
                cmd.Execute(command);
            }

            return Task.CompletedTask;
        }
    }
}
