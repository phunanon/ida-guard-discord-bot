namespace DiscordBot.Commands {

    public interface ISlashCommand {
        SlashCommandBuilder Command { get; }

        Task Execute(SocketSlashCommand command);
    }
}
