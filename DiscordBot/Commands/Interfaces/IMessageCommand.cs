namespace DiscordBot.Commands {

    public interface IMessageCommand {
        MessageCommandBuilder Command { get; }

        Task Execute(SocketMessageCommand command);
    }
}
