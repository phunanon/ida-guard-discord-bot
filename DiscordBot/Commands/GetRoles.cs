namespace DiscordBot.Commands {

    public class GetRoles : ISlashCommand {
        public SlashCommandBuilder Command => new SlashCommandBuilder()
            .WithName("get-roles")
            .WithDescription("Gets ones user specific roles.")
            .AddOption("user", ApplicationCommandOptionType.User, "The user whose roles you want to be listed.",
                isRequired: true);

        public async Task Execute(SocketSlashCommand command) {
            var guildUser = (SocketGuildUser)command.Data.Options.First().Value;
            var roleList = string.Join(",\n", guildUser.Roles.Where(x => !x.IsEveryone).Select(x => x.Mention));

            var embedBuilder = new EmbedBuilder()
                .WithAuthor(guildUser.ToString(), guildUser.GetAvatarUrl() ?? guildUser.GetDefaultAvatarUrl())
                .WithTitle("Roles")
                .WithDescription(roleList)
                .WithColor(Color.Green)
                .WithCurrentTimestamp();

            await command.RespondAsync(embed: embedBuilder.Build());
        }
    }
}
