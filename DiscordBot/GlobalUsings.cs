// System using directives
global using System;
global using System.Linq;
global using System.Collections.Generic;
global using System.Threading.Tasks;

// Discord.Net using directives
global using Discord;
global using Discord.WebSocket;
global using Discord.Commands;