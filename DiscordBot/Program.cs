﻿using System.Collections.Specialized;
using System.Configuration;

namespace DiscordBot {

    internal class Program {
        private readonly DiscordSocketClient _client = new();
        private readonly SlashCommandHandler _slashCommandHandler = new();
        private readonly MessageCommandHandler _messageCommandHandler = new();
        private readonly NameValueCollection _appSettings = ConfigurationManager.AppSettings;

        public static void Main(string[] args) => new Program().MainAsync(args).GetAwaiter().GetResult();

        private async Task MainAsync(string[] args) {
            _slashCommandHandler.StoreCommandsLocally();
            _messageCommandHandler.StoreCommandsLocally();

            _client.Log += Log;
            _client.SlashCommandExecuted += _slashCommandHandler.HandleCommandAsync;
            _client.MessageCommandExecuted += _messageCommandHandler.HandleCommandAsync;

            var token = _appSettings["BotToken"];

            await _client.LoginAsync(TokenType.Bot, token);
            await _client.StartAsync();

            _client.Ready += Ready;

            await Task.Delay(-1);
        }

        private static Task Log(LogMessage logMessage) {
            if (logMessage.Exception is CommandException commandException) {
                Console.WriteLine($"[Command/{logMessage.Severity}] {commandException.Command.Aliases[0]}" +
                                  $" failed to execute in {commandException.Context.Channel}.");
                Console.WriteLine(commandException);
            }
            else {
                Console.WriteLine($"[General/{logMessage.Severity}] {logMessage}");
            }

            return Task.CompletedTask;
        }

        private async Task Ready() {
            _client.Ready -= Ready;

            var guild = _client.GetGuild(Convert.ToUInt64(_appSettings["GuildID"]));
            await _slashCommandHandler.Install(_client, guild);
            await _messageCommandHandler.Install(_client, guild);
        }
    }
}
